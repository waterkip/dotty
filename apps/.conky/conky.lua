-- Conky functions in LUA
--

-- Simple printf, sprintf actually
sprintf = function(s,...)
  return (s:format(...))
end

function conky_show_disk(mount, desc)
    mnt_is   = sprintf("${if_mounted %s}", mount);
    mnt_not  = sprintf("$else %s is not mounted $endif", mount);
    mnt_info = sprintf("%s [%s]", mount, desc);
    mnt_size = sprintf("${fs_size %s}/${fs_used %s}", mount, mount);
    mnt_perc = sprintf("${fs_used_perc %s}", mount);
    mnt_bar  = sprintf("${fs_bar 7 %s}", mount);

    msg = "%s%s ${alignc}%s ${alignr}%s%%\n%s%s";
    return sprintf(msg, mnt_is, mnt_info, mnt_size, mnt_perc, mnt_bar, mnt_not);
end

function conky_show_net(int, desc, is_wlan)
    net_is = sprintf("${if_up %s}%s ", int, int);
    net_ip = sprintf("$alignr ${addrs %s}", int);
    net_not  = sprintf("${else}%s [%s] is not configured $endif", int, desc);
    net_graph="14,185";

    net_up_icon   = "${font PizzaDude Bullets:size=6}N${font}${voffset -2} ";
    net_up        = sprintf("${totalup %s}/${upspeed %s} $alignr ${upspeedgraph %s %s}", int, int, int, net_graph);
    net_up = net_up_icon .. net_up;

    net_down_icon = "${font PizzaDude Bullets:size=6}T${font}${voffset -2} ";
    net_down = sprintf("${totaldown %s}/${downspeed %s} $alignr ${downspeedgraph %s %s }", int, int, int, net_graph);
    net_down = net_down_icon .. net_down;

    if is_wlan == '1' then
        wifi_info = sprintf("${wireless_ap %s} $alignr ${wireless_bitrate %s}", int, int);
        wifi_ssid = sprintf("[${wireless_mode %s}] ${wireless_essid %s}", int, int);
        wifi_link = sprintf("${wireless_link_qual %s}/${wireless_link_qual_max %s}", int, int);
        wifi_perc = sprintf("${wireless_link_qual_perc %s}", int);
        wifi_bar  = sprintf("${wireless_link_bar %s}", int);

        net_info = sprintf("${alignc} %s $alignr %s\n%s ${alignr}%s %s%%\n%s\n",
        wifi_ssid, net_ip, wifi_info, wifi_link, wifi_perc, wifi_bar);
    else
        net_info = sprintf("%s $alignr %s\n", desc, net_ip);
    end
    return net_is .. net_info .. net_up .. "\n" .. net_down .. net_not;
end

function split(str, pat)
    local t = {}  -- NOTE: use {n = 0} in Lua-5.0
    local fpat = "(.-)" .. pat
    local last_end = 1
    local s, e, cap = str:find(fpat, 1)
    while s do
        if s ~= 1 or cap ~= "" then
            table.insert(t,cap)
        end
        last_end = e+1
        s, e, cap = str:find(fpat, last_end)
    end
    if last_end <= #str then
        cap = str:sub(last_end)
        table.insert(t, cap)
    end
    return t
end

function conky_mounted_disk()
    local cmd = "mount | egrep -wv 'devpts|securityfs|debugfs|devtmpfs|binfmt_misc|fusectl|sysfs|proc ' | awk '{print $3,$5}'"
    local Hnd, ErrStr = io.popen(cmd);

    local t
    local mount
    local res = ""
    if Hnd then
        for Line in Hnd:lines() do
            t = split(Line," ");
            mount = conky_show_disk(t [1], t [2])
            res = res .. mount .. ".\n"
        end
        Hnd:close()
    else
        print(ErrStr)
    end
    return res
end

