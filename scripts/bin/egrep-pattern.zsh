#!/bin/zsh

FILE=$1
REGEXP=""

for i in `cat $FILE`; do
	if [ -z "$REGEXP" ] ; then
		REGEXP=$i
	else
  		REGEXP="$i|$REGEXP"
	fi

done
echo ${REGEXP}
