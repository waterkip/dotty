#!/usr/bin/env perl
use warnings;
use strict;

use Data::Dumper;
use Getopt::Long;
use Pod::Usage;
use Try::Tiny;
use XML::Compile::Schema;
use Config::Any;

my %opt = (
    help    => 0,
    verbose => 0,
);

GetOptions(
    \%opt, qw(
        help
        xsd=s@
        element=s@
        print-index
        list-namespace
        xml
        validate=s
        verbose
        config=s
        )
) or pod2usage(1);

pod2usage(0) if ($opt{help});


if ($opt{config} && -f $opt{config}) {
    my $config = Config::Any->load_files(
        {
            files        => [$opt{config}],
            use_ext      => 1,
            flatten_hash => 1,

        }
    )->[0]{ $opt{config} };

    foreach (qw(element xsd)) {
        my $val = $opt{$_};
        next unless defined $config->{$_};
        my @config_val = ref $config->{$_} eq 'ARRAY'? @{$config->{$_}} : $config->{$_};

        if (ref $val eq 'ARRAY') {
            push(@{$opt{$_}}, @config_val);
        }
        elsif ($val) {
            unshift @config_val, $val;
            $opt{$_} = \@config_val;
        }
        else {
            push(@{$opt{$_}}, @config_val);
        }
    }
}

unless (defined $opt{xsd}) {
    warn "Missing option: xsd";
    pod2usage(1);
}

if ($opt{verbose}) {
    printf("Using XML::Compile::Schema::VERSION: %s\n", $XML::Compile::Schema::VERSION);
    printf("Using XSD: %s\n",  Dumper($opt{xsd}));
    printf("Using element: %s \n", Dumper($opt{element}));
}

my $schema = XML::Compile::Schema->new($opt{xsd});

if ($opt{validate}) {

    my ($element) = $opt{element} ? @{$opt{element}} : $schema->elements();

    try {
        my $reader = $schema->compile(READER => $element);
        my $data = $reader->($opt{validate});
        if ($opt{verbose}) {
            local $Data::Dumper::Terse      = 1;
            local $Data::Dumper::Indent     = 1;
            local $Data::Dumper::Varname    = "";
            local $Data::Dumper::Sparseseen = 0;
            local $Data::Dumper::Quotekeys  = 0;
            local $Data::Dumper::Sortkeys   = 1;
            print Dumper $data;
        }
        else {
            print "$opt{validate} validates against the XSD", $/;
        }
    }
    catch {
        die $_;
    };

}
elsif ($opt{element}) {
    my $type = $opt{xml} ? 'XML' : 'PERL';
    foreach (@{$opt{element}}) {
        print "\n" . $schema->template($type, $_, skip_header => 1) . "\n";
    }
}
elsif ($opt{'print-index' }) {
    $schema->printIndex;
}
elsif ($opt{'list-namespace'}) {
    print join("\n", $schema->namespaces()->list, '');
}
elsif ($opt{xml}) {
    my ($element) = $schema->elements();
    print "\n" . $schema->template('XML', $element, skip_header => 1) . "\n";
}
else {
    print join("\n", $schema->elements(), '');
}

1;

__END__


=head1 NAME

xsd-explain.pl - Explain XSD files

=head1 SYNOPSIS

    xsd-explain.pl OPTIONS

List all the elements

    ./dev-bin/xsd-explain.pl --xsd share/xsd/xential/buildservice.xsd \
    --xsd share/xsd/xential/jaxb.dev.java.net.array.xsd

=head1 OPTIONS

=over

=item * xsd

The path to the XSD you want to check, required. Multiples are allowed.

=item * element

The element you want to know more about, optional. Multiples are allowed.

=over

=item * xml

If you have supplied an element, you can choose a Perl or an XML data structure.
When not enabled we display a perl data structure

=back

=item * print_index

Prints the index of the schema

=item * list_namespace

List the namespaces

=item * validate

Validate the given XML, you need to supply an element

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
