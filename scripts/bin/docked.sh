#!/bin/sh

LAPTOP=eDP-1
DOCKED=HDMI-1

set_samsung() {
    xrandr --output $DOCKED --auto --primary
}

set_laptop_off() {
    xrandr --output $LAPTOP --off
}

set_samsung_off() {
    xrandr --output $DOCKED --off
}

set_laptop() {
    xrandr --output $LAPTOP --auto --primary
}

if [ -n "$1" ]
then
    set_$1
    if [ $1 = "laptop" ]
    then
        set_samsung_off
    else
        set_laptop_off
    fi
    exit $?
fi

xrandr | grep -w connected | grep -q $DOCKED
if [ $? -eq 0 ]
then
    set_samsung
    set_laptop_off
else
    set_laptop
    set_samsung_off
fi

$(dirname $0)/set-bg
