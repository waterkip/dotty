#!/usr/bin/perl
use warnings;
use strict;

use Getopt::Long;
use DateTime;
use DateTime::Format::ISO8601;
use Pod::Usage;

my %options = ();

GetOptions(
    \%options, qw(
        age=i
        date=s
        help|h
        )
);

if ($options{help}) {
    pod2usage({ verbose => 1, exitval => 0 });
}


if ($options{age}) {
    my $bday = $options{date}
        ? DateTime::Format::ISO8601->parse_datetime($options{date})
        : DateTime->now();
    my $dob = $bday->clone;
    $dob->subtract(years => $options{age});
    printf("Date of birth %s\n", $dob);
}
elsif ($options{date}) {
    my $bday = DateTime::Format::ISO8601->parse_datetime($options{date});
    my $now = DateTime->now();
    my $dur = $now - $bday;
    printf("Age %s\n", $dur->years);
}
else {
    pod2usage({ verbose => 1, exitval => 1 });
}

__END__

=head1 NAME

bday - A small cli script to calculate how old someone is

=head1 SYNOPSIS

bday OPTIONS

=head1 OPTIONS

=over

=item date

The birthday date, defaults to now: YYYYMMDD

=item age

The age of the person

=item help

This help

=back

=head1 COPYRIGHT and LICENSE

Wesley Schwengle, 2016

BSD license
