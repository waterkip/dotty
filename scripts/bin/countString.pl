#!/usr/bin/perl

use warnings;
use strict;

printf("%-30s %s\n", "word", "length");

foreach my $word (@ARGV) {
  my $length = length $word;
  printf("%-30s %s\n", $word, $length);
}

