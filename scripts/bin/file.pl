#!/usr/bin/perl

use File::LibMagic;
use Image::ExifTool qw(ImageInfo);
use Data::Dumper;
use File::MimeInfo::Magic;

my $magic = File::LibMagic->new();
my $pack_template = 'A22A40A2A25A2A40';

foreach (@ARGV) {
    _get_file_info($_);

}

sub _get_file_info {
    my $file = shift;

    my $info = $magic->info_from_filename($file);

    open my $fh, '<', $_;
    my $fh_info = $magic->info_from_handle($fh);

    my $exif_info = ImageInfo($file);
    my $exif_fh = ImageInfo($fh);

    print pack($pack_template,
        "Via libmagic (path)",
        $file, '', $info->{description}, '', $info->{mime_type}),
        $/;

    print pack($pack_template,
        "Via libmagic (fh)",
        $file, '', $fh_info->{description}, '', $fh_info->{mime_type}),
        $/;

    print pack($pack_template,
        "Via exiftool (fh)",
        $file, '', $exif_fh->{Application}, '', $exif_fh->{MIMEType}),
        $/;
    print pack($pack_template,
        "Via exiftool (file)",
        $file, '', $exif_info->{Application}, '', $exif_info->{MIMEType}),
        $/;

    my $mime_type = mimetype($file);
    my $mime_type_to = mimetype($fh);

    print pack($pack_template,
        "Via F::MI:M (fh)",
        $file, '', $mime_type_to, '', $mime_type_to),
        $/;
    print pack($pack_template,
        "Via F::MI:M (file)",
        $file, '', $mime_type, '', $mime_type),
        $/;

}


