#!/usr/bin/perl -w

use strict;

# In: filename (string)
# Action, removes all empty strings from the filename.
# Result: prints the output to the file

sub empty_string {
  my $file = shift;
  open(FH,"<$file");
  my $secondfile = ();

  while (<FH>) {
    my $line = $_;
    chomp($line);
    #
    # We also remove the lines which have only whitspace characters...
    #
    $line =~ s/^\s*$//g;
    if ($line !~ m/^$/) {
      push(@$secondfile, $line);
    }
  }
  close(FH);

  #
  # Overwrite the file, but now we have no empty lines..
  #
  open(OUT,">$file");
  foreach (@$secondfile) {
    print OUT $_ . "\n";
  }
  close(OUT);
}


foreach my $file (@ARGV) {
  print "Just removed all empty lines from file `$file` \n";
  empty_string($file);
}
