#!/usr/bin/env bash

# Check if we are on Ubuntu
if [ "$(lsb_release -is)" != "Ubuntu" ] ; then
    echo "This script is only intended to run on Ubuntu"
    exit 255
fi

# Check if we are root
if [ $UID -ne 0 ] ; then
    echo "Please run this as root!"
    exit 255
fi

# Get distro
distro=$(lsb_release -cs)

src=/etc/apt/sources.list

add_repo() {
    local repo=$1
    local sections="$2"
    shift;shift
    local desc="$@"
    grep -v "^#" $src | grep "$repo" | grep $distro &>/dev/null
    if [ $? -ne 0 ] ; then
        # Add them if not and update
        echo "" >> $src
        echo "## $desc" >> $src
        echo "deb $repo $distro $sections" >> $src
    fi
}

add_repo "http://extras.ubuntu.com/ubuntu" "main" "Extra repositories"
add_repo "http://archive.canonical.com/ubuntu" "partner" "Partner repositories"

aptitude update
# Finally, install all the packages
aptitude install acroread gstreamer0.10-ffmpeg ubuntu-restricted-extras vlc mplayer mozilla-plugin-vlc mozilla-mplayer libdvdcss2 non-free-codecs


# Make sure acrobat reader is the default app
cat <<OEF | tee -a $HOME/.local/share/applications/mimeapps.list

[Adobe acroread]
application/pdf=acroread.desktop
application/vnd.adobe.xfdf=acroread.desktop
application/vnd.fdf=acroread.desktop
application/vnd.adobe.xdp+xml=acroread.desktop
application/vnd.adobe.pdx=acroread.desktop
application/fdf=acroread.desktop
application/xdp=acroread.desktop
application/xfdf=acroread.desktop
application/pdx=acroread.desktop
OEF

