for i in $(echo "select datname from pg_database where datname ~ '\w{4}-'" | psql wesleys | grep -v -- "-------" | grep -vw datname | grep -v '(' ) ; do
    echo "drop database \"$i\"" | psql wesleys
done

for i in $(psql -l | grep 'wesleys' | awk '{print $1}'| egrep -v 'wesleys|template')
do
    echo "drop database \"$i\"" | psql wesleys
done
