#!/usr/bin/perl
use warnings;
use strict;

# TODO: Fix a general appconfig::opn subclass
use LWP::UserAgent;
use Data::Dumper;

my $lwp = LWP::UserAgent->new(
    ssl_opts => {
        SSL_ca_path => '/etc/ssl/certs'
    },
);

my $res = $lwp->get('https://dev.zaaksysteem.nl');
if (!$res->is_success) {
    print $res->status_line, $/;
    print Dumper $res;
}
else {
    print "Call succesful", $/;
}

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
