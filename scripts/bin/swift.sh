
src=$1
dest=$2

if [ -z "$src" ] || [ -z "$dest" ]
then
    echo "Unable to copy files!" 1>&2
    exit 255
fi

swift copy $src -d /$dest $(swift list $src)
