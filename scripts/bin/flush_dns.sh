
PIDS=$(pgrep dnsmasq)
if [ -n "$PIDS" ];
then
    echo "Restarting dnsmasq"
    sudo pkill -SIGHUP dnsmasq
fi

