#!/bin/bash

FLV=$1
AVI=$2

mencoder $1 -ofps 15 -vf scale=300:-2 -oac lavc -ovc lavc -lavcopts vcode c=msmpeg4v2:acodec=mp3:abitrate=64 -o $2
