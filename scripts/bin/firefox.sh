#/usr/bin/env bash
set -x

PATH=/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin
FF=/usr/lib/firefox-3.0.5/firefox

start_ff() {
    eval $FF -P $@ && disown
}

check_ff() {
    pgrep "$FF $1"
}

opt="Development -jsconsole -no-remote"
if [ -n "$1" ] ; then
    check_ff $opt
    start_ff $opt
else
    check_ff $opt
    opt="default"
    if [ $? -eq 0 ] ; then
        start_ff $opt -no-remote
    else
        check_ff $opt -no-remote
    fi

fi


