#!/bin/bash

mnt=/mnt/karmic-chroot
root_disk=/dev/sdb1
mnt_devices="proc dev dev/pts sys"

start() {
	sudo mkdir "$mnt"
	sudo mount $root_disk "$mnt"

	for i in $mnt_devices ; do
		sudo mount -o bind /$i "$mnt"/$i
	done

	sudo mv "$mnt"/etc/resolv.conf "$mnt"/etc/resolv.conf.orig
	sudo cp /etc/resolv.conf "$mnt"/etc/resolv.conf
	sudo chroot "$mnt" /bin/bash

}

stop() {
	for i in $mnt_devices; do
		sudo umount "$mnt"/$i
	done
	sudo mv "$mnt"/etc/resolv.conf.orig "$mnt"/etc/resolv.conf
	sudo umount "$mnt"
	sudo rmdir "$mnt"
}

$1


