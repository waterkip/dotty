#!/usr/bin/perl
use warnings;
use strict;

use SQL::Beautify;
use IO::All;
use Getopt::Long;
use Pod::Usage;
use autodie;

pod2usage(1) unless @ARGV;

my $pretty = SQL::Beautify->new(
    spaces => 2,
);

foreach (@ARGV) {
    my $line = io->file($_)->slurp;

    $line =~ s/\n//;
    $pretty->query($line);

    my $nice_sql = $pretty->beautify;

    open my $fh, '>', "$_";
    print $fh $nice_sql, $/;
    close($fh);
}

__END__

=head1 NAME

sql.pl - Pretty print SQL

=head1 SYNOPSIS

sql.pl file name

=head1 OPTIONS

=over

=item * --help (this help)

=back

=head1 AUTHOR

Wesley Schwengle

=head1 LICENSE and COPYRIGHT

Wesley Schwengle, 2016
