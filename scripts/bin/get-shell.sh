#!/usr/bin/env zsh

cd $HOME/.shell
git status | grep -q "^nothing to commit"
rc=$?
if [ $rc -ne 0 ]
then
    git stash -a
fi
git pull --rebase
if [ $rc -ne 0 ]
then
    git push
    git stash pop
fi

pass git pull --rebase
