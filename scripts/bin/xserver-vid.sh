#!/usr/bin/env bash

_prefix=xserver-xorg-video
KEEP=${_prefix}-vesa

if [ -z "$*" ] ; then
    echo "Usage: $(basename $0) [xserver-xorg-video package]"
    echo "eg. $(basename $0) intel vmware"
    exit 1
fi

# Based on what we find.
#ALSO_KEEP=$(sudo lshw -quiet -C display | grep vendor | awk '{print $2}' | tr [A-Z] [a-z])

for i in $@ $ALSO_KEEP; do
    KEEP="$KEEP ${_prefix}-$i"
done

_keep=$(echo $KEEP|sed -e 's/ /|/g')

remove=$(dpkg -l ${_prefix}-* | grep "^ii" | egrep -v "$_keep" | awk '{print $2}')

if [ -z "$remove" ] ; then
    sudo aptitude install $KEEP
    exit $?
fi

_keep=$(echo "$KEEP"| sed -e 's/ /\+ /g')

sudo aptitude purge $remove $_keep+

