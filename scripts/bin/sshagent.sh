
usage() {

    local SELF=$(basename $0);

    cat <<OEF
$SELF [ OPTIONS ]

OPTIONS:

  -h This help
  -a Run 'sudo update-alternatives --config ssh-askpass'
     (Debian/Ubuntu only!)
OEF

}

set_alternatives() {
  sudo update-alternatives --config ssh-askpass
}

while getopts "ah" name
do
    case $name in

        a) set_alternatives;;

        h) usage
           exit 0
           ;;

        *) echo "Unknown option" 1>&2
           exit 1;;

    esac
done
shift $((OPTIND -1))

export SSH_ASKPASS=${SSH_ASKPASS:-'/usr/bin/ssh-askpass'}
ssh-add $HOME/.ssh/id_rsa_ed25519 < /dev/null

# vim: filetype=zsh
