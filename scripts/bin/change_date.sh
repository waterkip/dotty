#!/bin/bash

stamp2date (){
    date --utc --date "1970-01-01 $1 sec" "+%Y-%m-%d %T"
}

date2stamp () {
        date --utc --date "$1" +%s
}

echo "Hay! Hoeveel seconden wil je alle files in deze map terugzetten?"
read aantal

for filename in "$@" ; do
    if [ -n "$filename" -a -e "$filename" -a -n "$aantal" ]; then
        touch -m -d@$(($(stat -c %Y "$filename") - $aantal)) "$filename"
    fi
done
