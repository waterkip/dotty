#!/usr/bin/perl
use warnings;
use strict;

use Getopt::Long;
use Pod::Usage;

use IO::Socket::SSL; # 'debug4';

my %options = (
    hostname => undef,
    port     => 443,
    proto    => 'tcp',
    scheme   => 'www',
    ca       => undef,
    ca_path  => undef,
    timeout  => 15,
    sni      => 0,
);

GetOptions(
    \%options, qw(help hostname=s port=i
        proto=s scheme=i ca=s timeout=i ca_path=s sni)
);

pod2usage(0) if $options{help};

$options{timeout} //= 15;

my $missing;
foreach (qw(hostname)) {
    if (!defined $options{$_}) {
        warn "$_ is not defined\n";
        $missing++;
    }
}

pod2usage(1) if $missing;

my $sock = IO::Socket::SSL->new(
    PeerPort            => $options{port},
    PeerHost            => $options{hostname},
    Timeout             => $options{timeout},
    Proto               => $options{proto},
    SSL_verify_mode     => SSL_VERIFY_PEER,
    SSL_verifycn_scheme => $options{scheme},
    $options{ca}      ? (SSL_ca_file  => $options{ca})       : (),
    $options{ca_path} ? (SSL_ca_path  => $options{ca_path})  : (),
    $options{sni}     ? (SSL_hostname => $options{hostname}) : (),
);

if (!$sock) {
    die IO::Socket::SSL::errstr;
}
else {
    if ($options{ca}) {
        print "SSL certificate '$options{ca}' for host $options{hostname} is correct\n";
    }
    else {
        print "SSL certificate for host $options{hostname} is correct\n";
    }
}


__END__

=head1 NAME

test_ssl.pl - Test an SSL certificate on a remote host

=head1 SYNOPSIS

test_ssl.pl --help [ OPTIONS ]

=head1 OPTIONS

=over

=item * --help

Show this help

=item * --hostname

The hostname you want to test

=item * --port

The remote port, defaults to C<443>

=item * --proto

The protocol used, defaults to C<tcp>

=item * --scheme

The SSL_verifycn_scheme used, defaults to C<www>

=item * --ca

The CA file you want to check

=item * --ca_path

Check the ca with the ca_path, when you include C</etc/ssl/certs> you
know the CA cert does not include the full chain

=item * --sni

SNI support

=back

=head1 AUTHOR

Wesley Schwengle

=head1 LICENSE and COPYRIGHT

Wesley Schwengle, 2016
