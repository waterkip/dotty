#!/usr/bin/perl
use warnings;
use strict;

my $text = $ARGV[0];

$text =~ s/\s+//g;
$text =~ s/(.{1,64})/$1\n/g;

print $text . "\n";
