
get_instance() {
    local label=$1

    echo $(docker ps -a | grep $label | grep -v '_run_' | awk '{print $1}')
}

clear_log() {
    local i
    local logfile

    for i in $*
    do
        logfile=$(docker inspect $i | jq -r '.[].LogPath')
        if [ -n "$logfile" ];
        then
            size=$(sudo stat --format %s -- $logfile)
            [ $size -gt 0 ] && sudo truncate -s 0 $logfile
        fi
    done
    return 0;

}

if [ -z "$1" ]
then
    echo "Usage: $(basename $0) docker-instance";
    exit 1
fi


if [ $1 = "ALL" ]
then

    for instance in $(docker ps -a  | awk '{print $1}' | grep -v CONTAINER)
    do
        clear_log $instance
    done
else
    for i in $*
    do
        instances=$(get_instance $i)

        if [ -z "$instances" ]
        then
            echo "Instance '$i' does not exist!" 1>&2
            continue
        fi

        clear_log $instances
    done
fi

exit 0;

