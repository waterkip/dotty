
set -x
set -e
CERTS=$HOME/certs

mkdir -p $CERTS

ROOT_KEY=$CERTS/root-ca.key
ROOT_PEM=$CERTS/root-ca.pem

if [ ! -e $ROOT_KEY ]
then
    openssl genrsa -out $ROOT_KEY 4096
fi
openssl req -x509 -new -nodes -key $ROOT_KEY \
    -sha256 -days 1024 -out $ROOT_PEM

HOST_KEY=$CERTS/host.key
HOST_CSR=$CERTS/host.csr
HOST_PEM=$CERTS/host.pem

openssl genrsa -out $HOST_KEY 4096
openssl req -new -key $HOST_KEY -out $HOST_CSR
openssl x509 -req -in $HOST_CSR -CA $ROOT_PEM -CAkey $ROOT_KEY \
    -CAcreateserial -out $HOST_PEM -days 1024 -sha256
