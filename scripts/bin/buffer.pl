#!/usr/bin/perl
use warnings;
use strict;

use Getopt::Long;
use DateTime;
use DateTime::Format::ISO8601;
use Pod::Usage;

my %options = (
    amount => 2689.53,
    months => 8,
);

GetOptions(\%options, qw(
    months=i
    amount=f
    help|h
));

if ($options{help}) {
    pod2usage({verbose => 1, exitval => 0});
}

foreach (1..$options{months}) {
    printf("Month %d: %.2f\n", $_, $_ * $options{amount});
}

__END__

=head1 NAME

buffer.pl - A small cli script to calculate how big your buffer should
be

=head1 DESCRIPTION

Tells you how big your buffer needs to be to survice X months.
Increments each months so you can set clear goals later on.

=head1 SYNOPSIS

buffer.pl OPTIONS

=head1 OPTIONS

=over

=item amount

Defaults to 2680.61

=item months

How many months do you want your buffer to be

=item help

This help

=back

=head1 COPYRIGHT and LICENSE

Wesley Schwengle, 2016

BSD license
