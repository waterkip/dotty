!#/bin/bash

KDE=.kde

KDE4=.kde4
KDE3=.kde3


cd $HOME

rm $KDE


kde3() {
    sudo ln -s $KDE3 $KDE
}

kde4() {
    sudo ln -s $KDE4 $KDE
}

sudo /etc/init.d/kdm stop
$1
sudo /etc/init.d/kdm start
