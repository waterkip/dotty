#!/bin/bash

PATH=/usr/bin/:/bin:/opt/conky/bin

## KDE 4.3
WALLPAPER=$(qdbus org.kde.plasma-desktop /MainApplication reparseConfiguration)
WALLPAPER=$(grep 'wallpaper=' $HOME/.kde/share/config/plasma-desktop-appletsrc | tail -1 | awk -F= '{print $NF}')
WALLPAPER=$(find $WALLPAPER/contents/images -name \*.png | sort | head -1)
## KDE3:
#WALLPAPER=$(dcop kdesktop KBackgroundIface currentWallpaper 1)

#WALLPAPER="$HOME/Pictures/aruba_2009/img_0235.jpg"


do_wallpaper() {
    GRAY_PAPER=$HOME/.conky/bg.png
    # Check md5 to see if we have to run convert
    F_MD5SUM=$GRAY_PAPER.md5
    MD5SUM=$(cat $F_MD5SUM 2>/dev/null)
    NEW_MD5=$(md5sum "$WALLPAPER" | awk '{print $1}')

    if [ -n "$NEW_MD5" ] && [ "$MD5SUM" != "$NEW_MD5" ] || [ ! -e "$GRAY_PAPER" ] ; then
        rm "$GRAY_PAPER"
        #$HOME/bin/convert-wallpaper.rb "$WALLPAPER" "$GRAY_PAPER"
        ln -s "$WALLPAPER" "$GRAY_PAPER"
        echo "$NEW_MD5" > $F_MD5SUM
    fi

    [ -r "$GRAY_PAPER" ] && DISPLAY=:0.0 feh --bg-scale "$GRAY_PAPER"
}

pgrep -x conky &>/dev/null
[ $? -ne 0 ] && conky
