PATH=$PATH:/usr/bin

wacom="Wacom Intuos PT M"
stylus="$wacom Pen stylus"
pad="$wacom Pad pad"
eraser="$wacom Pen eraser"
finger="$wacom Finger touch"

# On a new machine:
# apt install xserver-xorg-input-wacom xinput x11-xkb-utils
# then run this script

usage() {
    rc=$1
    [ -z "$rc" ] && rc=1;

    cat <<OEF
$(basename $0) OPTIONS

OPTIONS

    -h This help
    -i Initialize touchpad to desired settings
    -f Enable touchpad for fat fingers
    -s Enable stylus
    -t Toggle between stylus and finger

On a new machine you will need to have the following
dependencies installed:
xserver-xorg-input-wacom xinput x11-xkb-utils
OEF
    exit $rc;
}

touchpad() {
    [ -n "$DEBUG" ] && echo "setting touchpad"
    # Disable the stylus
    xinput disable "$stylus"

    # Enable the finger
    xsetwacom --set "$finger" Touch 'on'
    xsetwacom --set "$finger" Mode Relative

    #xsetwacom --set "$finger" Button 1 ""
    #xsetwacom --set "$finger" Button 2 ""
    #xsetwacom --set "$finger" Button 3 ""
    xsetwacom --set "$finger" ScrollDistance 40
}

stylus() {
    [ -n "$DEBUG" ] && echo "setting stylus"
    # Disable the finger
    xsetwacom --set "$finger" Touch 'off'

    xinput enable "$stylus"
    xsetwacom --set "$stylus" Mode Relative

    # Button 2 (middle mouse) map to left (1)
    xsetwacom --set "$stylus" Button 2 '1'
    # Button 1 (left mouse) map to right (3)
    xsetwacom --set "$stylus" Button 1 '3'

    xsetwacom --set "$stylus" Touch 'on'
    xsetwacom --set "$stylus" Threshold 300
}

pad() {
    [ -n "$DEBUG" ] && echo "setting pad"

    setxkbmap eu,us

    xsetwacom --set "$pad" Button 1 'key PgDn'
    xsetwacom --set "$pad" Button 2 ''
    xsetwacom --set "$pad" Button 3 'key PgUp'
    xsetwacom --set "$pad" Button 8 'key lsuper shift g'
    xsetwacom --set "$pad" Button 9 'key alt shift i'
}

toggle_touch() {
    pad
    xsetwacom --get "$finger" Touch | grep -q -- on
    if [ "$?" -eq 0 ]
    then
        [ -n "$DEBUG" ] && echo "Disabled touchpad"
        stylus;
    else
        [ -n "$DEBUG" ] && echo "Enabling touchpad"
        touchpad;
    fi
}

return 0;

while getopts "ihfstd" opts
do
    case $opts in
        i|s) pad; stylus; exit 0;;
        f) pad; touchpad; exit 0;;
        t) toggle_touch; exit 0;;
        h) usage 0 ;;
        d) DEBUG=1 ;;
    esac
done

usage 1
