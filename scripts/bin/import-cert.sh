#!/usr/bin/env zsh
#
# usage:  import-cert.sh remote.host.name [port]
#

do_certutil() {
    certutil -d $CERT_UTIL_STORE $@
}

ssl_fingerprint() {
    openssl x509 -fingerprint -noout $@
}

label=$1
file=$2

if [ -z "$label" ] || [ -z "$file" ]
then
    echo "Usage: $(basename $0) remote.host.name certificate.crt" 1>&2
    exit 1;
fi

if [ ! -f $file ]
then
    echo "Certificate file '$file' not found" >&2
    exit 2;
fi

if [ ! -d $CERT_UTIL_DIR ]
then
    mkdir -p $CERT_UTIL_DIR
    do_certutil -N
fi

do_certutil -L -n "$label" 2>&1 >/dev/null
if [ $? -eq 0 ]
then
    stored_hash=$(do_certutil -L -n "$label" -a | ssl_fingerprint)
    hash=$(ssl_fingerprint -in $file)
    if [ "$stored_hash" = "$hash" ]
    then
        echo "Same certificate, not changing a thing"
        exit 0
    else
        echo "Removing previous certificate"
        do_certutil -D -n "$label"
    fi
fi

do_certutil -A -t "C,," -n "$label" -i $file
