
iso=$1
dev=$2

if [ ! -f "$iso" ];then
    echo "No iso '$iso' found!";
    exit 2;
fi

if [ ! -b "$dev" ];then
    echo "No device '$dev' found!";
    exit 2;
fi

sudo dd bs=4M if=$iso of=$dev
sudo sync
