#!/usr/bin/perl -w

use strict;

$\="\n";

my @mac;
my $mac;
foreach (@ARGV) {
  print $_ ;
  chomp();
  @mac = split(/[^a-zA-Z0-9]/, $_);
  $mac = join("", @mac);
  printf("%s %s\n", lc($mac), uc($mac));

}

