#!/usr/bin/perl
use warnings;
use strict;

use YAML::Tiny;
use JSON::XS;
use Data::Dumper;

my $yaml = YAML::Tiny->read($ARGV[0]);

print Dumper $yaml;

my $json = JSON::XS->new->convert_blessed();

print $json->encode($yaml->[0]), $/;

