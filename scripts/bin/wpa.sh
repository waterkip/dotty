#!/bin/bash

SELF=`basename $0`
WPA=wpa_supplicant
PROGRAM=/sbin/${WPA}
CONF=/etc/${WPA}.conf
INTERFACE=wlan0
#DRIVER=iwl3945 # Intel Interfaces
#DRIVER=wext     # default linux driver
DAEMONMODE="-B"

function start() {

    OPTIONS="-c $CONF -i $INTERFACE $DAEMONMODE"
    if [ -n "$DRIVER" ]
    then
        OPTIONS="$OPTIONS -D $DRIVER"
    fi

    eval $PROGRAM $OPTIONS
}

function stop() {
    pkill $WPA
}

function debug() {
    stop
    DAEMONMODE="-ddd"
    start
}

function status() {
    pgrep -lf $WPA
}

case $1 in
    start|stop|debug|status)
        $1
        exit $?
    ;;
  *)
   echo "Usage: $SELF <start|stop>"
   exit 2
  ;;
esac
