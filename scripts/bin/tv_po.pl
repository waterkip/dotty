#!/usr/bin/perl

use strict;
use warnings;

use Getopt::Long;
use Data::Dumper;

my $opts;
my $self = `basename $0`;
chomp($self);

my $base_url = "mms://topstreams.omroep.nl/tv";
my $cmd = "/usr/bin/gxine --full-screen";

GetOptions(
  'omroep=s'    => \$opts->{omroep},
  'programma=s' => \$opts->{programma},
  'datum=s'     => \$opts->{datum},
  'date=s'      => \$opts->{datum},
  'url=s'       => \$opts->{url},
  'player=s'    => \$opts->{player},
  'debug'       => \$opts->{debug},
  'help'        => \$opts->{help},
);


sub Usage {
  print <<OEF

  Usage: $self --omroep *omroep* --programma *programmanaam* [--datum *datum*] [--url *url*] --player *path to player* [--debug] | --help

  --help     : Deze help
  --omroep   : De omroepnaam van de publieke omroep
  --programma: De programmanaam (zonder speciale karakters, en/of spaties)
  --datum    : yyyymmdd of laatste of indien het NOS journaal dag.tijdstip, default to: laatste
  --url      : Base url, defaults to $base_url
  --player   : Select the player you want to use, defaults to $cmd

  Do not send me any e-mails to report features (bugs).
OEF
}

sub Usage_exit {
  my $rc = shift;
  Usage();
  exit $rc;
}

Usage_exit(0) if $opts->{help};

if ( !defined $opts->{omroep} || !defined $opts->{programma} ) {
  Usage_exit(1);
}

$opts->{datum}  = "laatste" if ! defined $opts->{datum};
$opts->{url}    = $base_url if ! defined $opts->{url};
$opts->{player} = $cmd      if ! defined $opts->{player};

$opts->{url} = $base_url if ! defined $opts->{url};

$cmd = sprintf("$opts->{player} %s/%s/%s/bb.%s.asf\n", $opts->{url},$opts->{omroep},$opts->{programma}, $opts->{datum});
if ($opts->{debug}) {
  print Dumper $opts;
  print $cmd
}
system("$cmd");

exit 0;
