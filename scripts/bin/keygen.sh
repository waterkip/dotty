#!/usr/bin/zsh

genkey() {
    local keyfile=$1

    if [ -z "$keyfile" ]; then
        keyfile=$HOME/.ssh/id_rsa
    fi

    echo "Going to update/create keyfile $keyfile";

    if [ -e $keyfile ] ; then
        ssh-keygen -t ed25519 -b 4096 -f $keyfile -p
    else
        ssh-keygen -t ed25519 -b 4096 -f $keyfile
    fi
}

genkey $@
