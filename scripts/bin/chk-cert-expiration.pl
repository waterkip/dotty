#!/usr/bin/perl
use warnings;
use strict;

use Crypt::OpenSSL::X509;
use File::Find::Rule;
use DateTime;
use DateTime::Duration;
use Pod::Usage;
use Try::Tiny;

use Getopt::Long;
my %opt = (
    help       => 0,
    verbose    => 0,
    months     => 6,
    path       => '.',
);

{
    local $SIG{__WARN__};
    my $ok = eval {
        GetOptions(
            \%opt, qw(help verbose path=s cert=s months=i)
        );
    };
    if (!$ok) {
        pod2usage(1) ;
    }
}

pod2usage({ -verbose => 3, -exitval => 0 }) if ($opt{help});


my @files;
if ($opt{cert}) {
    if (! -f $opt{cert}) {
        die "File '$opt{cert}' not found\n";
    }
    @files = ($opt{cert});
    $opt{verbose} = 1;
}
else {
    my $path = $opt{path};
    @files = File::Find::Rule->file->name(qw[*.crt *.pem])->in($path);
}

my %certmap;
foreach my $file (@files) {
    my $rv = try {
        return Crypt::OpenSSL::X509->new_from_file($file);
    }
    catch {
        warn sprintf("Invalid: %s (%s)\n", $file, $_);
        return undef;
    };

    if ($rv) {
        $certmap{$file} = $rv;
    }
}

my $next_month = DateTime->today->add(months => ($opt{months} || 6));
my $next_month_seconds = $next_month->subtract_datetime_absolute(DateTime->today)->seconds;

for my $key (keys %certmap) {
    my $cert = $certmap{ $key };

    if ($cert->checkend(0)) {
        printf("Expired: %s (%s) %s\n", $key, $cert->notAfter, $cert->fingerprint_md5);
    }
    elsif ($cert->checkend($next_month_seconds)) {
        printf("Expires: %s (%s) %s\n", $key, $cert->notAfter, $cert->fingerprint_md5);
    }
    elsif ($opt{verbose}) {
        printf("Valid until: %s (%s) %s\n", $key, $cert->notAfter, $cert->fingerprint_md5);
    }
}

1;

__END__

=head1 NAME

chk-cert-expiration.pl - A perl script to check the expirations of an SSL cert

=head1 SYNOPSIS

chk-cert-expiration.pl [ --path /path/to/foo ] [ --cert /path/to/cert ] OPTIONS

=head1 OPTIONS

=over

=item help

This help

=item path

The path of the certificates, defaults to C<.>

=item months

Defines the number of months for which we warn when a certificate expires.
Defaults to 6.

=item verbose

Shows you addition information about the certificate

=item cert

The path to a single certificate so you can check a individual
certifcate without too much trouble

=back
