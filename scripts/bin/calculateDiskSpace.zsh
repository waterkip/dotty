#!/usr/local/bin/zsh

FILE=$1

TOTALSIZE=0

for file in `cat $FILE`
do
  FILESIZE=`du -k $file | awk '{print $1}'`
  TOTALSIZE=`expr $TOTALSIZE \+ $FILESIZE`
done

printf "Total size: %s k (equals) %s MB\n" $TOTALSIZE `expr $TOTALSIZE \/ 1024`

