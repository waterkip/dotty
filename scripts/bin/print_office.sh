
SELF=`basename $0`

# What we are determines what we do :)
if [ "$SELF" = "print_doc" ] ; then
  FUNC=print_word
elif [ "$SELF" = "print_excel" ] ; then
  FUNC=print_excel
fi

print_word() {
  antiword -f -a a4 "$1" | a2ps -
}

print_excel() {
  xlhtml "$1" | a2ps -1 -
}

# Fix for LANG settings which are not supported..
LANG_OLD=$LANG
unset LANG
for i in "$@" ; do
  $FUNC "$i"
done
# And back to the old values
export LANG=$LANG_OLD
