#!/usr/bin/env bash

location=$1
year=$2
genre=$3

O_IFS=$IFS
export IFS="
"

function sanitize_data() {
    echo "$@" | sed -e 's/[^[:alnum:]\&\=\(\)]/ /g' -e 's/[[:space:]]\+/ /g' -e 's/^[[:space:]]\+//'
}

if [ -d "$location" ] ; then
    location=$(echo $location | sed -e 's/^\.\///')
    for i in $(find $location -type f| sort) ; do

#        echo $i
#        continue

        artist_album=$(dirname $i)
        artist=$(sanitize_data $(echo $artist_album | cut -d\/ -f1))

        # Get the song info
        song=$(basename $i .mp3)
        song=$(basename $song .ogg)
        song=$(basename $song .flac)
        track=$(echo $song | awk -F\. '{print $1}')
        song=$(echo $song | sed -e 's/^[[:digit:]]\+.//')
        song=$(sanitize_data $song)

        album=$(echo $artist_album | cut -d\/ -f2)

        album=$(sanitize_data $album)

        #printf "Found: %s; Artist: %s; Album: %s; Song: %s\n" "$i" "$artist" "$album" "$song"
        #mp3info -n $track -t "$song" -a "$artist" -l "$album" "$i" -c "" -g "$genre" -y "$year"
        #mp3info -F "$i"
        id3v2 -l "$i"
        echo ""
        id3v2 -d "$i"
        id3v2 -1 -T $track -t "$song" -a "$artist" -A "$album" -c "" -g "$genre" -y "$year" "$i"
        #id3v2 -D "$id"
#        echo ""
        id3v2 -l "$i"
        echo ""
    done
fi


#  -h,  --help               Display this help and exit
#  -f,  --list-frames        Display all possible frames for id3v2
#  -L,  --list-genres        Lists all id3v1 genres
#  -v,  --version            Display version information and exit
#  -l,  --list               Lists the tag(s) on the file(s)
#  -R,  --list-rfc822        Lists using an rfc822-style format for output
#  -d,  --delete-v2          Deletes id3v2 tags
#  -s,  --delete-v1          Deletes id3v1 tags
#  -D,  --delete-all         Deletes both id3v1 and id3v2 tags
#  -C,  --convert            Converts id3v1 tag to id3v2
#  -1,  --id3v1-only         Writes only id3v1 tag
#  -2,  --id3v2-only         Writes only id3v2 tag
#  -a,  --artist  "ARTIST"   Set the artist information
#  -A,  --album   "ALBUM"    Set the album title information
#  -t,  --song    "SONG"     Set the song title information
#  -c,  --comment "DESCRIPTION":"COMMENT":"LANGUAGE"
#                            Set the comment information (both
#                             description and language optional)
#  -g,  --genre   num        Set the genre number
#  -y,  --year    num        Set the year
#  -T,  --track   num/num    Set the track number/(optional) total tracks
#
