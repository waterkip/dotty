#!/usr/local/bin/zsh

MAILTO=$1
FILE_LOCATIONS=$2
UUENC=`which uuencode`

if [[ ! -x $UUENC ]] ; then
  echo "could not find uuencode"
  exit 2;
fi

if [[ ! -d $FILE_LOCATIONS ]] ; then
  echo "Could not find $FILE_LOCATIONS, or its not a directory"
  exit 2;
fi

counter=0
for file in $FILE_LOCATIONS/*
do
  if [[ $counter == 0 ]] ; then
    echo "Mailing $file with counter $counter"
    $UUENC $file $file | mail -s "Test virus $counter: $file" $MAILTO
  fi
  counter=`expr $counter \+ 1`
done

# Always exit true
exit 0
