#!/usr/bin/perl

use warnings;
use strict;

my $self = `basename $0`;
chomp($self);
my $SWITCH = $ARGV[0] or Usage();
my $MAX = $SWITCH + 1;
my $FILE = $ARGV[1] or Usage();
my $prefix = $ARGV[2] or Usage();
my $counter = 0;
my $fileCount = 0;

my $DATE = `date +%Y%m%d`;
chop($DATE);

Usage() if ( ! -r $FILE );

open(FH, "<$FILE");

while (<FH>) {
	sortByNumber($_);
}
close(FH);

sub Usage {
  print "Usage: $self [MAX ENTRIES] [FILE] [PREFIX OF NEW FILES]\n";
  exit 2;
}


sub sortByNumber {
	my $line = shift;
	my $chomp = 0;
	chomp() if $chomp;

	if ($counter < $MAX) {
		open(OUT, ">>$prefix.$fileCount");
		print OUT $line;
		close(OUT);
		$counter++;
	}
	if($counter eq ($SWITCH)) {
		$fileCount++;
		$counter = 0;
	}


}
