IFS="
"
for i in $(file ~/.config/{google-chrome,chromium}*/*/* | grep SQLite | cut -d: -f1)
do
    sqlite3 $i .schema >/dev/null
    if [ $? -ne 0 ]
    then
        echo Errors with chrome file: $i
    fi
done
