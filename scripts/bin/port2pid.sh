#!/usr/bin/env bash
#

# Check all pids for this port, then list that process
for f in $(ls -1 /proc) ; do
    /usr/proc/bin/pfiles $f 2>/dev/null | /usr/xpg4/bin/grep -q "port: $1"
    if [ $? -eq 0 ] ; then
        echo -e "Port $1 is being used by PID: $(/usr/bin/ps -o pid -o args -p $f | sed 1d)"
        exit 0
    fi
done
exit 1

