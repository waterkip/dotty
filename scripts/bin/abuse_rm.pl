#!/usr/local/bin/perl -w
#
# hanst, Fri Jun 22 11:01:29 MET DST 2001
#
use strict;

my $roothome = '/home/';
my $location = undef;

sub usage () {
        print "Usage: abuse_rm <loginname> ...\n";
        exit 1;
}

sub chkusername ($) {
        my $username = shift @_;
        if ( $username =~ /[";<>*`|&\$#\/]/ ) {
                print "That's not a valid username. It contains strange characters.\n";
                exit 1;
        }
}

sub chklocation ($) {
        my $username = shift @_;
        $location = "/home/$username";
        if ($location =~ /[;<>*`|&\$#]/ ) {
                print "$location doesn't look like a /home dir...\n";
                exit 1;
        }
}

sub movedir ($) {
        my $location = shift @_;
        print "moving $location to /home/.removed ...\n";
        my $move=`mv $location /home/.removed`;
        print $move;
}


foreach (@ARGV) {
        chomp;
        my $username = $_;
        if (($username =~ /^\-+h/) or ($username eq "")) {
                usage();
        }
        chkusername ($username);
        chklocation($username);
        movedir($location);
}

exit 0;

