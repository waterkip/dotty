
usage() {
    local rc=$1

    [ -z "$rc" ] && rc=0;

    echo "Usage: $(basename $0) certificate.cer foo.pem";

    exit $rc;
}

while getopts "h" name
do
    case $name in
        h) usage;;
    esac
done
shift $((OPTIND - 1))

IN="$1"
shift;
OUT="$1"

if [ -z "$OUT" ]
then
    echo "You must supply an output file" 1>&2
    usage;
fi


if [ -z "$IN" ] || [ ! -f "$IN" ];
then
    echo "You must supply der or pkcs7 files!" 1>&2
    usage;
fi

( openssl pkcs12 -nodes -clcerts -in "$IN" -out "$OUT" 2>/dev/null ) \
    || \
( openssl pkcs7 -print_certs -in "$IN" -out "$OUT" 2>/dev/null ) \
    ||\
(
    openssl pkcs7 -inform DER -outform PEM -in "$IN" -print_certs \
        -out "$OUT" 2>/dev/null
) \
    ||\
(
    openssl x509 -inform der -in "$IN" -out "$OUT" 2>/dev/null
)
