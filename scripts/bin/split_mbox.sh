#!/usr/bin/env bash

[ -z "$1" ] || [ ! -e $1 ] && echo "Argh.. file name please.. " && exit 1
mkdir splitted

perl -pe 'BEGIN { $n=1 } open STDOUT, ">splitted/$ARGV.$n" and $n++ if /^From /' $1

