#!/usr/bin/perl
use warnings;
use strict;

use Getopt::Long;
use Pod::Usage;
no warnings 'recursion';

my %opts = (
    help      => 0,
    rate      => 4.15,
    loan      => 78400 + 105000,
    duration  => (65 - 43) * 12,
);


{
    local $SIG{__WARN__};
    my $ok = eval {GetOptions(\%opts, qw(help rate=f loan=f duration=i
        fixed liniair additional=f end=s savings=f))};
    if (!$ok) {
        die($@);
    }
}


pod2usage(0) if ($opts{help});

my @required = qw(loan rate);
foreach (@required) {
    if (!defined $opts{$_} || $opts{$_} <= 0) {
        pod2usage(1);
    }
}

my $ok = 0;
if (exists $opts{savings}) {
    $ok = 1;
};
my @optional = qw(fixed liniair);
foreach (@optional) {
    if (defined $opts{$_} && $opts{$_} > 0) {
        $ok++;
    }
}
if (!$opts{duration} and !$opts{end} || !$opts{start}) {
    $ok = 0;
}
pod2usage(1) unless $ok;

sub annuiteit_hypotheek {

    my $loan = shift;
    my $looptijd = shift;
    my $jaarrente = shift;

    return unless $looptijd;
    return unless $loan;

    my $maandrente = (1 + $jaarrente / 100) ** (1/12) - 1;
    my $maandbedrag = $loan * ( $maandrente / (1 -  ( ( 1 + $maandrente) ** ( -1 * $looptijd  ))));

    my $rente = $loan * $maandrente;
    my $aflossing = $maandbedrag - $rente;

    _print_data($loan, $rente, $aflossing, $maandbedrag, $looptijd);

    annuiteit_hypotheek($loan-$aflossing, $looptijd - 1, $jaarrente);
}

sub lineaire_hypotheek {
    my $loan = shift;
    my $looptijd = shift;
    my $jaarrente = shift;

    return unless $looptijd;
    return unless $loan;

    #my $maandrente = (1 + $jaarrente / 100) ** (1/12) - 1;
    my $maandrente = $jaarrente / 100/12;
    my $aflossing = $loan/$looptijd;

    my $rente = $loan * $maandrente;
    my $maandbedrag = $aflossing + $rente;

    _print_data($loan, $rente, $aflossing, $maandbedrag, $looptijd);

    lineaire_hypotheek($loan-$aflossing, $looptijd - 1, $jaarrente);
}

sub _print_data {
    my ($loan, $rente, $aflossing, $maandbedrag, $looptijd) = @_;

    printf(
        "Loan: %.2f\tInterest: %.2f\tPrincipal: %.2f\tTotal per month: %.2f%s\n",
        $loan, $rente, $aflossing, $maandbedrag,
        $looptijd % 12 == 0 ? ' Year passed' : ''
    );

}

if ($opts{fixed}) {
    annuiteit_hypotheek($opts{loan}, $opts{duration}, $opts{rate});
}
elsif ($opts{liniair}) {
    lineaire_hypotheek($opts{loan}, $opts{duration}, $opts{rate});
}
elsif (exists $opts{savings}) {
    bankspaar_hypotheek($opts{savings}, 0, $opts{duration}, $opts{rate});
}

sub bankspaar_hypotheek {

    my $inleg = shift;
    my $savings = shift // 0;
    my $looptijd = shift;
    my $jaarrente = shift;

    my $maandrente = $jaarrente / 100 /12;

    my $rente = $savings * $maandrente;
    $savings += $inleg;
    $savings += $rente;

    _print_data($savings, $rente, $inleg, $rente + $inleg, $looptijd);
    return unless $looptijd;
    bankspaar_hypotheek($inleg, $savings, $looptijd-1, $jaarrente);

}

__END__

=head1 NAME

mortgage.pl - A simple mortgage calculator

=head1 SYNOPSIS

mortgage.pl --loan 50000 --rate 4.15 [ OPTIONS ]

=head1 OPTIONS

=over

=item loan

The loan you have. Defaults to 183400.00

=item rate

The interest rate of the loan. Defaults to 4.15

=item duration

The duration of the loan in months, defaults to 360 months

=item fixed

Shows the mortgage in a fixed rate scenario. The montly payments stays
the same: The interest payments go down and the principal payments go up.

=item liniair

Shows the mortgage in a liniair rate scenario. The montly payments
differ per month: The principal payments stay the same each month

=back
