#!/usr/bin/perl
use warnings;
use strict;

use B qw/svref_2object/;

sub in_package {
    my ($coderef, $package) = @_;
    my $cv = svref_2object($coderef);
    return if not $cv->isa('B::CV') or $cv->GV->isa('B::SPECIAL');
    return $cv->GV->STASH->NAME;
}

sub list_module {
    my $module = shift;
    no strict 'refs';

    foreach my $m ( keys %{"$module\::"}) {
        if (defined &{"$module\::$m"}) {
            my $package = in_package(\&{*$m}, $module);
            print "$m is from $package", $/;
        }

    }
}

our @ISA;

use List::Util qw(any);
list_module('main');
