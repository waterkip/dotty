#!/usr/bin/env zsh

PATH=$PATH

#set -x

set -a zenders;

player_bin="mplayer"
MPLAYER_VERBOSE=9

add_player_opts() {
    player_opt="$player_opt $@"
}

function add_station() {
    local name="$1"
    local url="$2"
    local playlist="$3"
    if [ -n "$name" ] ; then
        local max=${#zenders[@]}
        max=$(($max + 1))
        zenders[$max]="$name,$url,$playlist"
    fi
}


radio_select() {

    # Define backtitle
    BACKTITLE="'Webradio by Wesley Schwengle'"

    # Define header text
    HEADER="'Select your station'"

    # Define box format, Height, Width, List Height
    SIZE_H=500
    SIZE_W=100

    local _max=${#zenders[@]}
    local freq=""
    local naam=""
    local zender=""
    local id=""

    local radio=""
    for ((i=1;i<=_max;i++)) ; do
        zender=${zenders[$i]}
        naam=$(echo $zender | cut -d "," -f 1)
        radio="$radio '$i' '$naam'"
    done
    echo $radio ;
    set -x
    choice=$(eval dialog --stdout --backtitle $BACKTITLE --menu $HEADER $SIZE_H $SIZE_W $_max $radio);
    retval=$?
    set +x

    case $retval in
        0) [ -n "$choice" ] && clear ; eval radio_start $choice;;
        1) clear; exit 0;;
        *) radio_select;;
    esac
}

function radio_start() {
    local id=$1
    local zender=""
    local url=""
    local playlist=""
    local naam=""

    zender="${zenders[$id]}"

    echo $zender | grep -q ","
    [ $? -ne 0 ]  && radio_select

    naam=$(echo $zender | cut -d "," -f 1)
    url=$(echo $zender | cut -d "," -f 2)
    playlist=$(echo $zender | cut -d "," -f 3)
    local player_opt_play=$player_opt
    if [ -n "$playlist" ] ; then
         if [ $playlist -eq 1 ] ; then
             player_opt_play="$player_opt_play -playlist"
         fi
    fi

    [ -z "$url" ] && radio_select

    playme="$player_bin ${player_opt_play} $url"
    printf "Now playing '%s' from '%s'\n" "$naam" "$url"
#    $playme &>/dev/null
#    echo "$playme"
    eval $playme
    radio_select
}


## Taken care of in $HOME/.mplayer/config
# Verbose or not
add_player_opts -quiet
add_player_opts -really-quiet
# No subtitles
add_player_opts -noautosub
# Volume control
#add_player_opts -softvol
#add_player_opts -volstep 1
#add_player_opts -volume 1
##add_player_opts -nocache
#add_player_opts -cache 1024
add_player_opts -cache-min 15
# at last
#add_player_opts -playlist


# Aruba
add_station "Cool FM" "http://www.coolaruba.com/stream/cool.m3u" 1
#add_station "Magic FM" "mms://wms1.iviplanet.com/magic965"
#add_station "Caliente 90.7 FM" "http://67.159.45.87:8214/;&type=mp3&volume=100"

add_station "Radio 1" "http://icecast.omroep.nl/radio1-bb-mp3" 0
add_station "Radio 2" "http://icecast.omroep.nl/radio2-bb-mp3" 0
add_station "Radio 3FM" "http://icecast.omroep.nl/radio3-bb-mp3" 0
add_station "Radio 4" "http://icecast.omroep.nl/radio4-bb-mp3" 0
add_station "Radio 5" "http://icecast.omroep.nl/radio5-bb-mp3" 0
add_station "Radio 6" "http://icecast.omroep.nl/radio6-bb-mp3" 0
add_station "Juize.nl" "http://82.201.100.9:8000/juizefm.m3u" 1
add_station "Radio 538" "http://82.201.100.9:8000/radio538.m3u" 1
add_station "KinkFM" "http://www.kinkfm.com/streams/kinkfm.m3u" 1
add_station "FunX - NL" "http://www.funx.nl/live/funx.asx" 1
add_station "FunX - Amsterdam" "http://www.funx.nl/live/amsterdam.asx" 1
add_station "FunX - Rotterdam" "http://www.funx.nl/live/rotterdam.asx" 1
add_station "FunX - Den Haag" "http://www.funx.nl/live/denhaag.asx" 1
add_station "FunX - Utrecht" "http://www.funx.nl/live/utrecht.asx" 1
add_station "FunX - Arab" "http://www.funx.nl/live/arab.asx" 1
add_station "FunX - Dance" "http://www.funx.nl/live/dance.asx" 1
#add_station "FunX - Fusion" "http://www.funx.nl/live/fusion.asx" 1
#add_station "FunX - Hiphop" "http://www.funx.nl/live/hiphop.asx" 1
add_station "FunX - Latin" "http://www.funx.nl/live/latin.asx" 1
add_station "FunX - Reggea" "http://www.funx.nl/live/reggae.asx" 1
add_station "FunX - Slow Jamz" "http://www.funx.nl/live/slowjamz.asx" 1
add_station "FunX - Desi" "http://www.funx.nl/live/desi.asx" 1
add_station "Deep Mix Moscow Radio" "http://deepmix.ru/deepmix128.pls" 1
add_station "Drum and Bass Radio" "http://www.dnbradio.com/hi.m3u" 1
add_station "Arrow Jazz FM" "http://www.garnierstreamingmedia.com/asx/streamerswitch.asp?stream=204" 1
add_station "Arrow Classic Rock" "http://www.garnierstreamingmedia.com/asx/streamerswitch.asp?stream=205" 1
add_station "Action Radio" "http://www.actionradio.nl/mp3.php" 1
add_station "RRRadio" "http://www.rrradio.nl:8000/rrradio_01.m3u" 1
add_station "Radio Totaal" "http://www.radiototaal.fm/luisteren/totaalwinamp.pls" 1
add_station "BNR" "http://www.garnierstreamingmedia.com/asx/bnrradio.asx" 1
add_station "Q-music" "http://www.q-music.nl/live/qmusic_winamp.pls" 1
add_station "SlamFM" "http://www.true.nl/streams/slamfmlivestream.pls" 1
add_station "RadioNL" "http://www.radionl.fm/stream/listen.asx" 1

# TV
add_station "Journaal 24" "http://livestreams.omroep.nl/nos/journaal24-bb"
add_station "Politiek 24" "http://livestreams.omroep.nl/nos/politiek24-bb"
add_station "Al Jazeera via NOS" "http://livestreams.omroep.nl/npo/mcr1"

radio_select
clear

