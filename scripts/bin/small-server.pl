#!/usr/bin/env perl
use warnings;
use strict;

use Data::Dumper;
use Getopt::Long;
use HTTP::Status qw(status_message);
use IO::All;
use Pod::Usage;
use HTTP::Request::Params;
use File::Spec::Functions qw(catfile);
use YAML::Tiny;
use JSON::XS qw(decode_json);

my %options = (
    return_code => 200,
    config      => catfile($ENV{HOME}, qw( .config small-server server.conf)),
);

{
    local $SIG{__WARN__};
    my $ok = eval {
        GetOptions(
            \%options, qw(help port=i answer=s
                content_type=s return_code=i keep_open! ssl=s ssl_key=s
                ssl_crt=s wait=i headers! config=s)
        );
    };
    if (!$ok) {
        die($@);
    }
}

pod2usage(0) if $options{help};


if (-f $options{config}) {
    my $contents = YAML::Tiny->read($options{config})->[0];

    foreach (keys %$contents) {
        $options{$_} = $contents->{$_};
    }

    if ($contents->{endpoints}) {
        foreach (keys %{ $contents->{endpoints} }) {
            $contents->{endpoints}{$_}{qr} = qr/$_/;
        }
        $options{endpoints} = $contents->{endpoints};
    }

}

$options{port} //= 9001;
$options{ssl}  //= 0;


my $d;
if ($options{ssl}) {
    use HTTP::Daemon::SSL qw(inet4);
    $d = HTTP::Daemon::SSL->new(
        LocalAddress => $options{ssl},
        LocalPort    => $options{port},
        ReuseAddr    => 1,
        $options{ssl_key} ? (SSL_key_file  => $options{ssl_key}) : (),
        $options{ssl_crt} ? (SSL_cert_file => $options{ssl_crt}) : (),
    );
}
else {
    use HTTP::Daemon;
    $d = HTTP::Daemon->new(
      LocalAddress => '0.0.0.0',
      LocalPort => $options{port}, ReuseAddr => 1
    );
}

if (!$d) {
    die "Unable to start a HTTP deamon on port $options{port}\n";
}

printf("Please contact me at '%s'\n", $d->url);

my $counter = 0;

my $answer = undef;

use Image::ExifTool qw(ImageInfo);

sub _get_file {
    my $file = shift;

}

sub _answer_parsing {
    my $data = shift;

    return ($data->{content}, $data->{'content-type'})
      if exists $data->{content};

    my $file = $data->{file};

    my $answer;
    if (-f $file) {
        $answer = io->catfile($file)->slurp;
    }
    else {
        $file = catfile($ENV{HOME}, $file);
        $answer = io->catfile($file)->slurp;
    }
    my $info = ImageInfo($file);
    return ($answer, $info->{MIMEType});
}

if ($options{answer}||$options{content}) {
    my $ct;
    ($answer, $ct) = _answer_parsing(\%options);
    $options{content_type} = $ct;
}

$options{content_type} //= 'text/plain';
$options{answer}      = $answer;
$options{return_code} = 200;

$Data::Dumper::Varname  = "";
$Data::Dumper::Sortkeys = 1;

my $ct         = undef;
my $rc         = undef;
my $rc_message = undef;

while (1) {
    while (my $c = $d->accept) {
        my $pid = fork();

        # We are going to close the new connection on one of two conditions
        #  1. The fork failed ($pid is undefined)
        #  2. We are the parent ($pid != 0)
        if (!defined $pid || $pid == 0) {
            $c->close;
            print "Needs close: $pid\n";
            next;
        }

        while (my $r = $c->get_request) {

            $rc     = undef;
            $answer = undef;

            my $content_type = $r->header('content-type') // 'text/plain';

            #if ($content_type eq 'text/plain') {
                print $r->as_string, $/;
            #}
            #else {
            #    my $x = $r->clone();
            #    my $json = JSON::XS::decode_json($r->content);
            #    print Dumper %{$json};
            #}

            if ($options{wait}) {
                print "Waiting for $options{wait} seconds", $/;
                sleep $options{wait};
            }


            if (my $soapaction = $r->header('soapaction')) {
                $soapaction =~ s/(?:^\"|\"$)//g;
                if (my $action = $options{'soap-action'}{$soapaction}) {
                    if ($action->{reference}) {
                        my $action = $options{'soap-action'}{$action->{reference}};
                        ($answer, $ct) = _answer_parsing($action);
                    }
                    elsif ($action->{file}) {
                        ($answer, $ct) = _answer_parsing($action);
                    }
                    else {
                      warn "Configured soap action '$soapaction' but no response can be given\n";
                    }
                    $rc         = $action->{return_code};
                    $rc_message = $action->{return_code};
                }
                else {
                    warn "Undefined soap action '$soapaction'\n";
                    warn sprintf("Available soap actions:\n%s\n", join($/, sort keys %{$options{'soap-action'}}));
                }
            }
            elsif ($options{endpoints}) {
                ENDPOINTS: foreach (sort keys %{ $options{endpoints} }) {
                    if ($r->uri eq $_ || $r->uri =~ /$options{endpoints}{$_}{qr}/) {
                        ($answer, $ct)
                            = _answer_parsing($options{endpoints}{$_});
                        $rc         = $options{endpoints}{$_}{return_code};
                        $rc_message = $options{endpoints}{$_}{return_code};
                        last ENDPOINTS;
                    }
                }
            }
            $answer //= $options{answer};
            $rc     //= $options{return_code};

            print $answer // '<no answer defined>', $/;

            $rc_message //= status_message($rc);

            $c->send_response(
                HTTP::Response->new(
                    $rc,
                    $rc_message // "Undefined HTTP code $rc",
                    ['Content-Type' => $ct // $options{content_type}],
                    $answer // '',
                )
            );
            $counter++;
        }
        $c->close;
        undef($c);
    }
    print "Next loop";
    print $/;
}
print "The end is near\n";

__END__

=head1 NAME

small-server.pl - A small server app

=head1 SYNOPSIS

small-server.pl --port 90

=head1 OPTIONS

=over

=item port

The port you want the server to listen on

=item answer

Send a simple answer back to the client

=item wait

Wait X seconds before sending an answer, defaults to 0

=item content_type

Define the content_type of the answer

=item ssl

Set the hostname for SSL usage

=item ssl_key

Set the ssl key used for the SSL server (pem format)

=item ssl_crt

Set the ssl cert used for the SSL server (pem format)

=item return-code

Set the return code of the response

=item keep-open

Keep the connection alive

=item headers

Dump the headers of the request

=back

=head1 AUTHOR

Wesley Schwengle

=head1 LICENSE and COPYRIGHT

This code is placed in the public domain.

Wesley Schwengle, 2015
