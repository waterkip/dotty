if [ $# -eq 0 ]
then
  echo "$(basename $0) \"path/to/api?query\" CURL_OPTS" >&2
  exit 1;
fi

tokens=gitlab/tokens
#tokens=mintlab/gitlab

path="$1"
shift;
curl -s \
  -H "PRIVATE-TOKEN: $(pass $tokens | grep 'Full access token' | awk '{print $NF}')" \
  $@ \
  "https://gitlab.com/api/v4/$path"
