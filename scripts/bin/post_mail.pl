#!/usr/bin/perl
use warnings;
use strict;

use Getopt::Long;
use LWP::UserAgent;
use HTTP::Cookies;
use HTTP::Request::Common;
use IO::All;
use Data::Dumper;
use Pod::Usage;

my %options = (
    content_type => 'form/data',
    k            => 0,
    timeout      => 60,
    d            => 0,
    X            => 'POST'
);

{
    local $SIG{__WARN__} = sub { die shift };
    my $ok = eval {
        GetOptions(\%options,
            qw(help endpoint=s mail=s content_type=s timeout=i k d v));
    };
    if (!$ok) {
        die($@);
    }
}

if ($options{help}) {
    pod2usage({ -verbose => 1 });
}

my $missing;
foreach (qw(mail endpoint)) {
    if (!$options{$_}) {
        $missing = 1;
        warn "Required option $_ missing\n";
    }
}
if ($missing) {
    pod2usage({ -verbose => 1, -exit => 1 });
}

my $ua = LWP::UserAgent->new(
    agent             => "How about no",
    ssl_opts          => { SSL_ca_path => '/etc/ssl/certs', },
    cookie_jar        => HTTP::Cookies->new(),
    protocols_allowed => [qw(https http)],
    timeout           => $options{timeout},
);

if ($options{k}) {
    $ua->ssl_opts(
        verify_hostname => 0,
        SSL_verify_mode => 0,
    );
}

my $req = POST $options{endpoint},
    Content_Type => 'multipart/form-data',
    Content      => [message => scalar io->catfile($options{mail})->slurp];

my $res = $ua->request($req);

if ($res->is_success) {
    print $res->decoded_content, $/;
}
else {
    my $req = $req->as_string;
    die $req . $/ . $res->as_string, $/;
}

__END__

=head1 NAME

post_mail.pl - Upload MIME files to an endpoint

=head1 SYNOPSIS

post_mail.pl OPTIONS

=head1 OPTIONS

=over

=item endpoint

The end point you want to talk to. Required

=item content_type

Defaults to C<form/data>. Optional.

=item mail

Path to MIME message

=back

=head1 AUTHOR

Wesley Schwengle

=head1 LICENSE and COPYRIGHT

This code is placed in the public domain.

Wesley Schwengle, 2017
