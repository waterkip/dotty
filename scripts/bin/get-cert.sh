# usage:  get-cert.sh remote.host.name [port]

if [ -z "$1" ]
then
    echo "Usage: $(basename $0) hostname portnumber" >&2
    echo "" >&2
    echo "Leaves a file behind named 'hostname.pem'" >&2
    exit 1;
fi

get_cert() {
    local hostname=$1
    local port=${2:-443}

    echo "$(echo | openssl s_client -connect ${hostname}:${port} 2>&1 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p';)"
}

get_cert $@ | tee $1.pem
