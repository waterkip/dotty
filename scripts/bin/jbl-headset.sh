####  Resources:
set -x

##  Why this is needed
# https://jimshaver.net/2015/03/31/going-a2dp-only-on-linux/

##  My original question
# https://askubuntu.com/questions/1004712/audio-profile-changes-automatically-to-hsp-bad-quality-when-i-change-input-to/1009156#1009156

##  Script to monitor plugged earphones and switch when unplugged (Ubuntu does that, but nice script):
# https://github.com/freundTech/linux-helper-scripts/blob/master/padevswitch/padevswitch
# run `pacmd list-sinks  | grep '\(name:\|alias\)'` and fill with value from alias of your bluetooth device

BLUETOOTH_MAC="34:DF:2A:55:D4:6B"
BLUETOOTH_DEVICE="bluez_output.$(echo $BLUETOOTH_MAC | sed -e 's/:/_/g')"
PROFILE_A2DP="a2dp_sink"
PROFILE_HEADSET_UNIT="headset_head_unit"
PROFILE_HANDSFREE_UNIT="handsfree_head_unit"


resetBT() {
  sudo rfkill block bluetooth && sleep 0.1 && sudo rfkill unblock bluetooth;
  exit;
}

get_list_item() {
  local type=$1
  shift;
  pactl list $type | grep 'Name:' | sed -rn 's/\s*Name: (.*?)/\1/p'

}

get_sinks() {
  get_list_item sinks
}

sink_name() {
  get_sinks | grep -w "$1" | head -1
}

card_name() {
  get_list_item cards | grep -w "bluez" | head -1
}

get_sink_name() {
  SINK_NAME=$(sink_name "${BLUETOOTH_DEVICE}")
}

assert_sink_name() {
  get_sink_name

  if [ -z "${SINK_NAME}" ]
  then
    echo "${BLUETOOTH_DEVICE} headset not found." >&2
    exit 1;
  fi

}

get_current_profile() {
  assert_sink_name

  if $(echo "${SINK_NAME}" | grep -q "${PROFILE_A2DP}") ; then
    echo "Quality sound mode";
  else
    echo "Phone mode"
  fi
  return;

}

toggle() {
  assert_sink_name

  if $(echo "${SINK_NAME}" | grep -q "${PROFILE_A2DP}") ; then
    speak
  else
    listen
  fi
}

listen() {
  assert_sink_name

  ## Switch the output to that.
  pacmd set-default-sink "${SINK_NAME}"

  #### Change profile to quality output + no mic.:
  CARD=$(card_name)
  pacmd set-card-profile "${CARD}" "${PROFILE_A2DP}"
  return;
}

speak() {
  ## Change profile to crappy output + mic.:
  CARD=$(card_name)

  local sinks=$(get_sinks)

  local sink_set=0

  # Newer version
  pacmd set-card-profile "${CARD}" "${PROFILE_HANDSFREE_UNIT}"
  if [ $? -eq 0 ]
  then
    sink_set=1
  else
    pacmd set-card-profile "${CARD}" "${PROFILE_HEADSET_UNIT}"
    [ $? -eq 0 ] && sink_set=1
  fi


  if [ $sink_set -eq 0 ]
  then
    echo "Unable to set card profile(s)" >&2
    exit 1;
  fi

  assert_sink_name
  pacmd set-default-source "${SINK_NAME}.monitor" || echo 'Try `pacmd list-sources`.';
}

usage_exit() {
  cat <<OEF
  Usage: $(basename $0) OPTIONS

  -h  This help
  -c  Connect bluetooth
  -C  Disconnect bluetooth
  -p  Phone mode
  -a  Quality sound mode
  -t  Toggle between phone and quality mode
  -s  Status
  -R  Reset BT

OEF
}

connect_bluetooth() {
  bluetoothctl connect $BLUETOOTH_MAC
}

disconnect_bluetooth() {
  bluetoothctl disconnect $BLUETOOTH_MAC
}

while getopts "hcCpatsR" opts
do
    case $opts in
        h) usage_exit;;
        c) CONNECT=1;;
        C) DISCONNECT=1;;
        p) PHONEMODE=1;;
        a) QSOUNDMODE=1;;
        t) MODE_TOGGLE=1;;
        s) STATUS=1;;
        R) RESET=1;;
    esac
done

if [ ${RESET:-0} -eq 1 ]
then
  resetBT
  exit 0;
fi

if  [ ${STATUS:-0} -eq 1 ]
then
  get_current_profile
  exit 0
fi

if [ ${CONNECT:-0}  -eq 1 ]
then
  connect_bluetooth
elif  [ ${DISCONNECT:-0} -eq 1 ]
then
  disconnect_bluetooth
fi

if [ ${PHONEMODE:-0} -eq 1 ]
then
  speak
elif [ ${QSOUNDMODE:-0} -eq 1 ]
then
  listen
elif [ ${MODE_TOGGLE:-0} -eq 1 ]
then
  toggle
  get_current_profile
fi

echo "$CONNECT $DISCONNECT $PHONEMODE $QSOUNDMODE $MODE_TOGGLE" | grep -q 1 || usage_exit



