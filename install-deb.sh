#!/bin/sh

set -e

apt-get update
apt-get install --no-install-recommends -y \
  $(sed -e 's/#.*//' debian-pkglist)
