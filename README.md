# Dotfile directory for Wesley Schwengle

# INSTALL

The first thing you want to install is stow, this task is automated by running

  ./install-stow.sh

This will install stow in your `$HOME/.local` directory. You can also opt to
install stow from your local package manager.

Then you'll need to run

  ./install-perl.sh

if you have any perl dependencies declared in your cpanfile. And finally you'll
have to run setup-env.sh to install all of your things.
You can run setup-env.sh if you have updated your repository, or just use the
`stow` command to add/delete things with `stow` directly.

# Updating your repository

When your repository is updated with only existing files you don't need to run
`stow`, as `stow` creates symlinks. So everything gets updated.

# LICENSE

All the source code in this repository is covered by the MIT license.
