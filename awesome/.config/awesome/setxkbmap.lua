-- https://awesome.naquadah.org/wiki/Change_keyboard_maps

kbdcfg = {}
kbdcfg.cmd = "setxkbmap"
kbdcfg.layout = { { "eu", "" , "EU" }, {"us", "", "US" } }
--kbdcfg.layout = { { "eu", "" , "EU" }, { "eu", "dvorak" , "EU-dvorak" }, {"us", "", "US" }, {"us", "dvorak", "US-dvorak" } }

kbdcfg.current = 1

kbdcfg.widget = wibox.widget.textbox()
kbdcfg.widget:set_text(" " .. kbdcfg.layout[kbdcfg.current][3] .. " ")

kbdcfg.switch = function ()
    kbdcfg.current = kbdcfg.current % #(kbdcfg.layout) + 1
    local t = kbdcfg.layout[kbdcfg.current]
    kbdcfg.widget:set_text(" " .. t[3] .. " ")
    os.execute( kbdcfg.cmd .. " " .. t[1] .. " " .. t[2] )
end

-- Mouse bindings
kbdcfg.widget:buttons(
    awful.util.table.join(awful.button({ }, 1, function ()
    kbdcfg.switch() end))
)

right_layout:add(kbdcfg.widget)
