-- Always:
-- dual-monitor detection
--
local r = require("runonce");

local exec_apps = {
    dual_screen        = '/home/wesleys/bin/office.sh',
    wacom_and_keyboard = '/home/wesleys/bin/set_input_devices.sh -i',
}

local spawn_apps = {
    network           = 'nm-applet',
    sshagent          = homedir .. '/bin/sshagent.sh',
    --konsole           = 'konsole',
    --browser           = 'google-chrome-unstable'
    --conky             = homedir .. '/bin/conky.sh',
}

for label, app in pairs(spawn_apps) do
    r.run(app)
end

for label, app in pairs(exec_apps) do
    os.execute(app)
end
