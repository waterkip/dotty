FROM registry.gitlab.com/waterkip/stowing:latest

COPY debian-pkglist .
RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install --no-install-recommends -y \
        $(sed -e 's/#.*//' debian-pkglist)

COPY perl/.perlenv perl/.perlenv
COPY cpanfile .
COPY install-perl.sh .
RUN ./install-perl.sh

COPY . .
RUN ./setup-env.sh
