#!/usr/bin/env bash

PATH=$HOME/.local/bin:$PATH
source perl/.perlenv

cpanm -n IPC::System::Simple
cpanm File::BaseDir
cpanm --installdeps .
