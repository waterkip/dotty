#!/usr/bin/env bash

set -e

if [ -e .stow-debian ]
then
    sudo apt-get install stow
    exit 0;
fi

PATH=$HOME/.local/bin:$PATH
source perl/.perlenv

cd build
cpanm --local-lib=$PERL_LOCAL_LIB_ROOT local::lib Module::Build IO::Scalar
cpanm --installdeps .
